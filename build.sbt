import Dependencies._

lazy val root = (project in file("."))
  .settings(
    organization in ThisBuild := "com.example",
    version      in ThisBuild := "0.1.0-SNAPSHOT",
    name := "jedha_exercise",
    libraryDependencies += scalaTest % Test
  )
